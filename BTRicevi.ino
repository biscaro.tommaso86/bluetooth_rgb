/* Riceve messaggi che vengono inviati via Bluetooth HC-05 da una app.
*/

#include <SoftwareSerial.h>
int rossoPin = 11;
int verdePin = 10;         //numero dei pin per collegare il led rgb
int bluPin = 9;
const int RXPin = 2;  // da collegare su TX di HC05
const int TXPin = 3;  // da collegare su RX di HC05
const int ritardo = 100;

//creo una nuova porta seriale via software
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);

char msgChar;

void setup()
{
  pinMode(rossoPin, OUTPUT);
  pinMode(verdePin, OUTPUT);
  pinMode(bluPin, OUTPUT);
  pinMode(RXPin, INPUT);
  pinMode(TXPin, OUTPUT);

  myBT.begin(9800);

  Serial.begin(9800);
}

void loop()

{ char gg[9];
  int i = 0;
  //while(!myBT.available());
  while (myBT.available()) {

    msgChar = char(myBT.read());
    Serial.print(msgChar);
    gg[i] = msgChar;   //inserisco in gg un carattere alla volta msgChar
    i++;

  }
  delay(ritardo);
  String r; String v; String b;   //stringhe usate per salvare i valori rgb di ogni colore
  r = gg[0];
  r += gg[1];
  r += gg[2];
  v = gg[3];
  v += gg[4];                    //inserisco nelle stringhe i valori estraendoli da gg
  v += gg[5];                         
  b = gg[6];
  b += gg[7];
  b += gg[8];
  rgb(r.toInt(), v.toInt(), b.toInt());      //richiamo il metodo rgb passando in input il valore intero dell variabili r(Rosso),v(Verde),b(Blu) 
}

void rgb(int rosso, int verde, int blu) {      //metodo rgb usato per colorare il led rgb 
  analogWrite(rossoPin, rosso);
  analogWrite(verdePin, verde);
  analogWrite(bluPin, blu);
}
