package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.UUID;

public class SubActivity extends AppCompatActivity {
   public static BluetoothAdapter bluetoothAdapter;
   // private final int REQUEST_ENABLE_BT = 1;
    public static String listElement;
    public static BluetoothDevice targetDevice;
    public static BluetoothSocket btSocket;
    public static OutputStreamWriter writer;
    //nt ConnessioneOK=-1;

   // private static final int INTENT_CODE =1 ;
    LinearLayout llCerchio;
    SeekBar seekBar1;
    SeekBar seekBar2;
    SeekBar seekBar3;
    Button ritorna;
    String finale="mondo"; //inizializzo la stringa che conterrà i dati da passare all'arduino
    int c;   //variabile usata per cambiare il colore del cerchio

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
        seekBar1.setOnSeekBarChangeListener(seekBarChangeListener); //controlla il colore rosso
        seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        seekBar2.setOnSeekBarChangeListener(seekBarChangeListener2);  //controlla il colore blu
        seekBar3 = (SeekBar) findViewById(R.id.seekBar3);
        seekBar3.setOnSeekBarChangeListener(seekBarChangeListener3);    //controlla il colore verde
    }
    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    String s =Integer.toString (progress);
                    TextView txt1 = (TextView) findViewById(R.id.textView2);    //mostro nella casella di testo rossa il valore per il colore rosso
                    txt1.setText(s);
                     c = 0xff000000 + 0x10000 *seekBar1.getProgress()+0x100*seekBar3.getProgress()+seekBar2.getProgress();
                    myCircle.setColor(c);       //coloro il cerchio
                    try {
                        finishd();                //richiamo il metodo finishd per inviare i dati all'arduino
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {


                }

            };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener2 =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    seekBar2.getProgress();
                    String s =Integer.toString (progress);
                    TextView txt1 = (TextView) findViewById(R.id.textView3);    //mostro nella casella di testo blu il valore per il colore blu
                    txt1.setText(s);
                     c = 0xff000000 + 0x10000 *seekBar1.getProgress()+0x100*seekBar3.getProgress()+seekBar2.getProgress();
                    myCircle.setColor(c);          //coloro il cerchio
                    try {
                        finishd();
                    } catch (IOException e) {               //richiamo il metodo finishd per inviare i dati all'arduino
                        e.printStackTrace();
                    }
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };
    private SeekBar.OnSeekBarChangeListener seekBarChangeListener3 =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    seekBar3.getProgress();
                    String s =Integer.toString (progress);
                    TextView txt1 = (TextView) findViewById(R.id.textView1);       //mostro nella casella di testo verde il valore per il colore verde
                    txt1.setText(s);
                    c = 0xff000000 + 0x10000 *seekBar1.getProgress()+0x100*seekBar3.getProgress()+seekBar2.getProgress();
                    myCircle.setColor(c);             //coloro il cerchio
                    try {
                        finishd();
                    } catch (IOException e) {
                        e.printStackTrace();                    //richiamo il metodo finishd per inviare i dati all'arduino
                    }
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };
    public void finishd() throws IOException {
        String d=Integer.toString(seekBar1.getProgress()); //Stringa contenente i valori per il rosso
        String c1=Integer.toString(seekBar2.getProgress());  //Stringa contenente i valori per il blu
        String c2=Integer.toString(seekBar3.getProgress());  //Stringa contenente i valori per il verde

        //effettuo i controlli per verificare che il dato sia sempre di lunghezza= 3 (quindi se il valore è minore di 100 aggiungo uno 0
        // e se è minore di 10 aggiungo 00 all'inizio della stringa
        if(seekBar1.getProgress()<100){
            d="0"+d;
            if(seekBar1.getProgress()<10){
                d="0"+d;
            }
        }
        if(seekBar2.getProgress()<100){
            c1="0"+c1;
            if(seekBar2.getProgress()<10){
                c1="0"+c1;
            }
        }
        if(seekBar3.getProgress()<100){
            c2="0"+c2;
            if(seekBar3.getProgress()<10){
                c2="0"+c2;
            }
        }
        finale=d+c2+c1+"\r\n"; //Stringa che contiene  i valori decimali dei colori da inviare
        writer.write(finale);        //invio dei dati all'arduino
        writer.flush();
    }
}
